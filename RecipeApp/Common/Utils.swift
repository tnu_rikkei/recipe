//
//  Utils.swift
//  RecipeApp
//
//  Created by Tung Phan on 17/12/2021.
//

import UIKit

struct Utils {
    
    static var isLandscape: Bool {        
        return UIDevice.current.orientation.isLandscape
    }
    
}


